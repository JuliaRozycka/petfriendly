<%--
  Created by IntelliJ IDEA.
  User: Julia
  Date: 10/04/2020
  Time: 13:54
  To change this template use File | Settings | File Templates.
--%>
<!doctype html>
<html lang="en">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"
         import="java.util.*,edu.ib.petfriendlyproject.Place" %>
<head>
    <title>Update place</title>
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">PetFriendly</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor03"
                aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor03">
            <ul class="navbar-nav me-auto">
                <li class="nav-item">
                    <a class="nav-link active" href="index.html">Home
                        <span class="visually-hidden">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="client_view.jsp">Check out available objects
                        <span class="visually-hidden">(current)</span>
                    </a>
                </li>
            </ul>

            <form class="d-flex">
                <a class="btn btn-primary btn-lg" href="admin_login.html" role="button">Admin</a>
            </form>
        </div>
    </div>
</nav>

<h1 style="margin-left: 10px; margin-top: 10px">Edit place info:</h1>
<div class="container" style="width: 600px">
    <form action="AdminServlet" method="get">
        <?php $selected = $_GET['select'];?>
        <input type="hidden" name="command" value="UPDATE"/>
        <input type="hidden" name="placeID" value="${PLACE.id}"/>
        <div class="form-group">
            <label for="Name">Name</label>
            <input type="text" class="form-control" name="nameInput" id="Name" value="${PLACE.name}"/>
        </div>
        <div class="form-group">
            <label for="exampleSelect1" class="form-label mt-4">Type:</label>
            <select class="form-select" id="exampleSelect1" name="typeInput"/>
            <option value="Hotel" <c:if test="${PLACE.type == 'Hotel'}">selected</c:if>>Hotel</option>
            <option value="Restaurant" <c:if test="${PLACE.type == 'Restaurant'}">selected</c:if>>Restaurant</option>
            <option value="Bar" <c:if test="${PLACE.type == 'Bar'}">selected</c:if>>Bar</option>
            <option value="Café" <c:if test="${PLACE.type == 'Café'}">selected</c:if>>Caf&eacute;</option>
            <option value="Hairdresser" <c:if test="${PLACE.type == 'Hairdresser'}">selected</c:if>>Hairdresser</option>
            <option value="Motel" <c:if test="${PLACE.type == 'Motel'}">selected</c:if>>Motel</option>
            <option value="Shop" <c:if test="${PLACE.type == 'Shop'}">selected</c:if>>Shop</option>
            <option value="Bar" <c:if test="${PLACE.type == 'Bar'}">selected</c:if>>Bar</option>
            <option value="Shopping mall" <c:if test="${PLACE.type == 'Shopping mall'}">selected</c:if>>Shopping mall
            </option>
            <option value="Grocery store" <c:if test="${PLACE.type == 'Grocery store'}">selected</c:if>>Grocery store
            </option>
            <option value="Shopping mall" <c:if test="${PLACE.type == 'Shopping mall'}">selected</c:if>>Shopping mall
            </option>
            <option value="Supermarket" <c:if test="${PLACE.type == 'Supermarket'}">selected</c:if>>Supermarket</option>
            <option value="Library" <c:if test="${PLACE.type == 'Library'}">selected</c:if>>Library</option>
            <option value="Other" <c:if test="${PLACE.type == 'Other'}">selected</c:if>>Other</option>
            </select>
        </div>
        <div class="form-group">
            <label for="Street">Street</label>
            <input style="margin-top: 6px" id="Street" type="text" class="form-control" name="streetInput" value="${PLACE.street}"/>
            <input style="margin-top: 6px" id="City" type="text" class="form-control" name="cityInput" value="${PLACE.city}"/>
            <input style="margin-top: 6px" id="Zip" type="text" class="form-control" name="zipInput" value="${PLACE.zip}"/>
            <input style="margin-top: 6px" id="Country" type="text" class="form-control" name="countryInput" value="${PLACE.country}"/>
        </div>
        <div class="form-group">
            <label for="email" class="form-label mt-4">Email address</label>
            <input type="email" class="form-control"
                   id="email" placeholder="Enter email" name="emailInput"
                   value="${PLACE.email}">
        </div>
        <div class="form-group">
            <label for="Phone">Phone number:</label>
            <input id="Phone" type="text" class="form-control" name="phoneInput" value="${PLACE.phone}"/>
        </div>
        <div class="form-group">
            <label for="exampleTextarea" class="form-label mt-4">Description:</label>
            <textarea class="form-control" id="exampleTextarea" rows="3"
                      name="descriptionInput">${PLACE.description}</textarea>
        </div>
        <br>
        <button type="submit" class="btn btn-info">Edit data</button>
    </form>
</div>


<div class="row">
    <div class="container-fluid">

        <div class="col-sm-9">
            <a href="AdminServlet" class="btn btn-lg btn-primary" role="button" aria-disabled="true">Go back</a>
        </div>
    </div>
</div>

</body>
</html>

