<%--
  Created by IntelliJ IDEA.
  User: Julia
  Date: 10/04/2020
  Time: 14:38
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"
         import="java.util.*,edu.ib.petfriendlyproject.Place" %>

<sql:query var="rs" dataSource="jdbc/petfriendly">
    select * from places
</sql:query>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Our places</title>
</head>

<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/main.css">

<body>

<div class="container-image">
    <img src="img/clouds_v2.jpg" alt="clouds" style="width:100%;">
    <div class="top-left" style="background-color: rgba(0,0,0,0.2)">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <a class="navbar-brand" href="index.html" style="color: white">PetFriendly</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor03"
                        aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarColor03">
                    <ul class="navbar-nav me-auto">
                        <li class="nav-item">
                            <a class="nav-link active" href="index.html" style="color: white">Home
                                <span class="visually-hidden">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="about_us.html" style="color: white">About us
                                <span class="visually-hidden">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="contact.html" style="color: white">Contact
                                <span class="visually-hidden">(current)</span>
                            </a>
                        </li>
                    </ul>

                    <form class="d-flex">
                        <a class="btn btn-primary btn-lg" href="admin_login.html" role="button">Admin</a>
                    </form>
                </div>
            </div>
        </nav>
    </div>
    <div class="top-left" style="margin-top: 80px">
        <div class="row form-group"></div>
        <div class="row form-group"></div>
        <div class="row form-group"></div>
        <div class="row form-group"></div>


        <h1 style="color: whitesmoke">Our places</h1>
        <div style="margin-top: 10px; margin-bottom: 20px; position: absolute; margin-left: 1%">
            <input id='filterInput' onkeyup='filterTable()' type='text'
                   style=" width: 160px; height: 38px; border: none; font-size: 20px">
            <select id="selectInput" style="font-size: 20px; color: white; height: 40px; background-color: #9fcdff">
                <option value="Name">Name</option>
                <option value="Type">Type</option>
                <option value="Address">Address</option>
                <option value="Description">Description</option>
            </select>
        </div>
        <div class="row form-group"></div>
        <div class="row form-group"></div>
        <div class="row form-group"></div>

        <table class="table table-light" id="tableInput" style="margin-top: 60px">

            <thead style="background-color: #d4e8ff">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Type</th>
                <th scope="col">Address</th>
                <th scope="col">Email</th>
                <th scope="col">Phone</th>
                <th scope="col">Description</th>
            </tr>
            </thead>
            <tbody>

            <c:forEach var="tmpPlace" items="${rs.rows}">


                <tr>
                    <th scope="row">${tmpPlace.id}</th>
                    <td>${tmpPlace.name}</td>
                    <td>${tmpPlace.type}</td>
                    <td>${tmpPlace.street}, ${tmpPlace.city} ${tmpPlace.zip}, ${tmpPlace.country}</td>
                    <td>${tmpPlace.email}</td>
                    <td>${tmpPlace.phone}</td>
                    <td>${tmpPlace.description}</td>
                </tr>


            </c:forEach>
            </tbody>
        </table>

        <div class="row form-group"></div>
        <div class="row form-group"></div>
        <div class="row form-group"></div>
    </div>
</div>

<script>
    function filterTable() {
        // Declare variables
        var input, filter, table, tr, td, i, txtValue, select, index, selectValue;
        select = document.getElementById("selectInput");
        input = document.getElementById("filterInput");
        filter = input.value.toUpperCase();
        selectValue = select.value.toUpperCase();
        table = document.getElementById("tableInput");
        tr = table.getElementsByTagName("tr");

        if (selectValue === "NAME") {
            index = 0;
        }

        if (selectValue === "TYPE") {
            index = 1;
        }

        if (selectValue === "ADDRESS") {
            index = 2;
        }

        if (selectValue === "DESCRIPTION") {
            index = 5;
        }

        // Loop through all table rows, and hide those who don't match the search query
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[index];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
</script>

</body>
<footer>
    Julia Różycka
    PetFriendly Sp.Z.O.O.<br>
    petfriendly@example.com
</footer>
</html>
