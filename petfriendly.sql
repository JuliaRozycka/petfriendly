create database petfriendly;
use petfriendly;

drop table places;
create table places(
id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
name varchar(50) NOT NULL,
type enum('Restaurant','Café','Hotel','Bar','Hairdresser','Motel','Shopping mall','Grocery store','Supermarket','Shop','Library','Other'),
street varchar (50) NOT NULL,
city varchar (50) NOT NULL,
zip varchar (10) NOT NULL,
country varchar (50) NOT NULL,
email varchar (50),
phone varchar (15),
description varchar (200)
);

INSERT into places VALUES
('1','Paw hotel','Hotel','ul. Nyska 19','Wrocław','50-566','Poland', 'pawhotel@example.com','+48987654321','We are a luxury hotel in the Wrocław city centre. Come and see what we have to offer!'),
('2','Dziunia bar','Bar','ul. Wyzwolenia 19/1', 'Racibórz', '47-480', 'Poland', 'dziuniabar@example.com','+48123456789','Come and visit our bar with your beloved pet. We offer free bag of treats to our new clients!'),
('3','Cat café','Café','ul. Bardzka 56', 'Warszawa', '00-400', 'Poland', 'catcafe@example.com',null,'We are a small cafe in a big city. You are more than welcome to bring your cats along to our place.'),
('4','Parrot cut','Hairdresser','Parrot St. 3', 'New York','48400',  'USA',null,null,'Cool hairstyles for pirates. You may bring your parrot with you, arrr!'),
('5','Paw petrol','Other','ul. Ropy Naftowej 50', 'Złotniki', '00-000', 'Poland', 'pawpetrol@example.com','+11000555666','Gas station in Złotniki city center.'),
('6','Unicorn mall','Shopping mall','Warcroft St. 20/5','Zabawkowo', '70-700' ,'Bajkolandia', null,'+66006666600','Magical shopping mall in made up country ;)');

SELECT * FROM places;


DELIMITER $$
CREATE PROCEDURE add_Admin(IN p_Name VARCHAR(30), IN p_Passw VARCHAR(30))
BEGIN
    DECLARE `_HOST` CHAR(14) DEFAULT '@\'localhost\'';
    SET `p_Name` := CONCAT('\'', REPLACE(TRIM(`p_Name`), CHAR(39), CONCAT(CHAR(92), CHAR(39))), '\''),
    `p_Passw` := CONCAT('\'', REPLACE(`p_Passw`, CHAR(39), CONCAT(CHAR(92), CHAR(39))), '\'');
    SET @`sql` := CONCAT('CREATE USER ', `p_Name`, `_HOST`, ' IDENTIFIED BY ', `p_Passw`);
    PREPARE `stmt` FROM @`sql`;
    EXECUTE `stmt`;
    SET @`sql` := CONCAT('GRANT SELECT,UPDATE,INSERT,DELETE ON petfriendly.places TO ', `p_Name`, `_HOST`);
    PREPARE `stmt` FROM @`sql`;
    EXECUTE `stmt`;
    DEALLOCATE PREPARE `stmt`;
    FLUSH PRIVILEGES;
END$$



