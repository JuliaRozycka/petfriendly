package edu.ib.petfriendlyproject;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Class used to make connection with database with admin.
 */
public class DBUtilAdmin extends DBUtil{

    /**
     * URL address of database
     */
    private String URL;
    /**
     * Name of user
     */
    private String name;
    /**
     * User password
     */
    private String password;

    /**
     * Simple constructor for url address.
     *
     * @param URL
     */
    public DBUtilAdmin(String URL) {
        this.URL = URL;
    }

    /**
     * Method used to list the places from the database.
     *
     * @return list of places
     * @throws Exception if connection fails
     */
    @Override
    List<Place> getPlace() throws Exception {

        List<Place> places = new ArrayList<>();

        Connection conn = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            //połączenie z BD
            conn = DriverManager.getConnection(URL,name,password);
            statement = conn.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM places");

            while (resultSet.next()){
                places.add(new Place(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("type"),
                        resultSet.getString("street"),
                        resultSet.getString("city"),
                        resultSet.getString("zip"),
                        resultSet.getString("country"),
                        resultSet.getString("phone"),
                        resultSet.getString("email"),
                        resultSet.getString("description")
                ));
            }

        } finally {
            close(conn,statement,resultSet);
        }

        return places;
    }

    /**
     * Method used to add a new place to the list of places.
     *
     * @param place a new class Place object
     * @throws Exception if connection fails
     */
    public void addPlace(Place place) throws Exception{
        Connection conn = null;
        PreparedStatement ps = null;

        try{
            conn = DriverManager.getConnection(URL,name,password);
            String sql = "INSERT INTO places(name,type,street,city,zip,country,email,phone,description) "+
                    "VALUES(?,?,?,?,?,?,?,?,?)";
            ps = conn.prepareStatement(sql);
            ps.setString(1, place.getName());
            ps.setString(2, place.getType());
            ps.setString(3, place.getStreet());
            ps.setString(4, place.getCity());
            ps.setString(5, place.getZip());
            ps.setString(6, place.getCountry());
            ps.setString(7, place.getEmail());
            ps.setString(8, place.getPhone());
            ps.setString(9, place.getDescription());

            ps.execute();
        } finally {
            close(conn,ps,null);
        }
    }

    /**
     * Method used to get specific place identified by its unique id.
     *
     * @param id unique key
     * @return specific place
     * @throws Exception if connection fails
     */
    public Place getPlace(String id) throws Exception{
        Place places = null;

        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try{
            int placeID = Integer.parseInt(id);
            conn = DriverManager.getConnection(URL,name,password);
            statement = conn.prepareStatement("SELECT * FROM places WHERE id=?");
            statement.setInt(1,placeID);
            resultSet = statement.executeQuery();

            if(resultSet.next()){
                places = new Place(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("type"),
                        resultSet.getString("street"),
                        resultSet.getString("city"),
                        resultSet.getString("zip"),
                        resultSet.getString("country"),
                        resultSet.getString("phone"),
                        resultSet.getString("email"),
                        resultSet.getString("description")
                );
            } else {
                throw new Exception("Cannot find id: "+placeID);
            }

            return places;

        } finally {
            close(conn,statement,resultSet);
        }
    }

    /**
     * Method used to update information about specific place (by its ID).
     *
     * @param place Place class object
     * @throws Exception if connection fails
     */
    public void updatePlace(Place place) throws Exception{
        Connection conn = null;
        PreparedStatement statement = null;

        try{
            conn = DriverManager.getConnection(URL,name,password);
            String sql = "UPDATE places SET name=?, type=?, street=?,city=?,zip=?,country=?,email=?,phone=?,description=? where id=?";

            statement = conn.prepareStatement(sql);
            statement.setString(1, place.getName());
            statement.setString(2, place.getType());
            statement.setString(3, place.getStreet());
            statement.setString(4, place.getCity());
            statement.setString(5, place.getZip());
            statement.setString(6, place.getCountry());
            statement.setString(7, place.getEmail());
            statement.setString(8, place.getPhone());
            statement.setString(9, place.getDescription());
            statement.setString(10,String.valueOf(place.getId()));
            statement.execute();
        } finally {
            close(conn,statement,null);
        }


    }

    /**
     * Method used to delete specific place (by its ID).
     *
     * @param id unique key
     * @throws Exception if connection fails
     */
    public void deletePlace(String id) throws Exception{
        Connection conn = null;
        PreparedStatement statement = null;

        try{
            int placeId = Integer.parseInt(id);
            conn = DriverManager.getConnection(URL,name,password);
            String sql = "DELETE FROM places WHERE id=?";
            statement = conn.prepareStatement(sql);
            statement.setInt(1,placeId);
            statement.execute();
        } finally {
            close(conn,statement,null);
        }
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
