package edu.ib.petfriendlyproject;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * Class used to make connection with database.
 */
public abstract class DBUtil {
    abstract List<Place> getPlace() throws Exception;

    /**
     * Method closing the connection between java and database.
     *
     * @param conn connection
     * @param statement query statement
     * @param resultSet result set query
     */
    protected static void close(Connection conn, Statement statement, ResultSet resultSet) {
        try {
            if (resultSet != null)
                resultSet.close();
            if (statement != null)
                statement.close();
            if (conn != null)
                conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
