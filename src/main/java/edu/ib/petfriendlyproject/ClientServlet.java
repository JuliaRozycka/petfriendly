package edu.ib.petfriendlyproject;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Class used for processing HTTP request for ordinary user.
 */
@WebServlet(name = "ClientServlet", value = "/ClientServlet")
public class ClientServlet extends HttpServlet {

    /**
     * Interface source used to make connection with database
     */
    private final DataSource dataSource;
    /**
     * Class used to make connection with database
     */
    private DBUtilClient dbUtilClient;

    /**
     * Constructor used to make a connections with database not identified by name and password.
     *
     * @throws NamingException
     * @throws SQLException
     */
    public ClientServlet() throws NamingException, SQLException {
            Context initCtx = new InitialContext();
            Context envCtx = (Context) initCtx.lookup("java:/comp/env");
            dataSource = (DataSource) envCtx.lookup("jdbc/petfriendly");
    }

    /**
     * Initialization method creating a connection.
     *
     * @throws ServletException
     */
    @Override
    public void init() throws ServletException {
        super.init();

        try{
            dbUtilClient = new DBUtilClient(dataSource);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Method used to list places in client view.
     *
     * @param httpServletRequest
     * @param httpServletResponse
     * @throws Exception
     */
    private void listPlaces(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        List<Place> places = dbUtilClient.getPlace();
        httpServletRequest.setAttribute("PLACES_LIST", places);
        RequestDispatcher dispatcher = httpServletRequest.getRequestDispatcher("/client_view.jsp");
        dispatcher.forward(httpServletRequest,httpServletResponse);
    }

    /**
     * Method corresponding with HTTP method 'GET'. Gets command produced by the user.
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try{
            listPlaces(request,response);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Empty method for 'POST' request - ordinary user does not need it.
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
