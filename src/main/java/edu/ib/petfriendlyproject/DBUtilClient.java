package edu.ib.petfriendlyproject;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Class used to make connection with database with ordinary user not identified by username and password.
 */
public class DBUtilClient extends DBUtil {

    /**
     * An interface used for connection with database - alternative to DriverManager.
     */
    private final DataSource dataSource;

    /**
     * Simple constructor.
     *
     * @param dataSource
     */
    public DBUtilClient(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Method used to list all of the places.
     *
     * @return list of places
     * @throws Exception if connection fails
     */
    @Override
    List<Place> getPlace() throws Exception {
        List<Place> places = new ArrayList<>();

        Connection conn = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            //połączenie z BD
            conn = dataSource.getConnection();
            statement = conn.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM places");

            while (resultSet.next()){
                places.add(new Place(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("type"),
                        resultSet.getString("street"),
                        resultSet.getString("city"),
                        resultSet.getString("zip"),
                        resultSet.getString("country"),
                        resultSet.getString("phone"),
                        resultSet.getString("email"),
                        resultSet.getString("description")
                ));
            }

        } finally {
            close(conn,statement,resultSet);
        }

        return places;
    }
}
