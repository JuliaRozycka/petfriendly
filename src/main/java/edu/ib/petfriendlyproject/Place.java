package edu.ib.petfriendlyproject;

/**
 * Class used to store information about a place.
 */
public class Place {
    /**
     * Place ID
     */
    private int id;
    /**
     * Name od the place
     */
    private String name;
    /**
     * Type of the place
     */
    private String type;
    /**
     * Address of place
     */
    private String street;
    private String city;
    private String zip;
    private String country;

    /**
     * Places' phone number
     */
    private String phone;
    /**
     *Place's email
     */
    private String email;
    /**
     * Short description
     */
    private String description;

    /**
     * A constructor with ID.
     *
     * @param id
     * @param name
     * @param type
     * @param street
     * @param city
     * @param zip
     * @param country
     * @param email
     * @param phone
     * @param description
     */
    public Place(int id, String name, String type, String street, String city, String zip, String country, String phone, String email, String description) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.street = street;
        this.city = city;
        this.zip = zip;
        this.country = country;
        this.phone = phone;
        this.email = email;
        this.description = description;
    }
    /**
     * Constructor without ID.
     *
     * @param name
     * @param type
     * @param city
     * @param street
     * @param zip
     * @param country
     * @param email
     * @param phone
     * @param description
     */
    public Place(String name, String type, String street, String city, String zip, String country, String phone, String email, String description) {
        this.name = name;
        this.type = type;
        this.street = street;
        this.city = city;
        this.zip = zip;
        this.country = country;
        this.phone = phone;
        this.email = email;
        this.description = description;
    }




    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Place{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", street='" + street + '\'' +
                ", city='" + city + '\'' +
                ", zip='" + zip + '\'' +
                ", country='" + country + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
