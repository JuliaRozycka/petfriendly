package edu.ib.petfriendlyproject;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

/**
 * Class used for processing HTTP request for admin.
 */
@WebServlet(name = "AdminServlet", value = "/AdminServlet")
public class AdminServlet extends HttpServlet {

    /**
     * Connection with database
     */
    private DBUtilAdmin dbUtil;
    /**
     * Database url
     */
    private final String url = "jdbc:mysql://localhost:3306/petfriendly?useSSL=false&allowPublicKeyRetrieval=true&"+
            "serverTimezone=CET";

    /**
     * Servlet initialization - where the connection is made.
     *
     * @param config
     * @throws ServletException
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        try {
            dbUtil =  new DBUtilAdmin(url);
        } catch (Exception e){
            throw new ServletException(e);
        }
    }

    /**
     * Method corresponding with HTTP method 'GET'. Gets command produced by the user.
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String command = request.getParameter("command");

            if(command == null)
                command = "LIST";
            switch (command) {
                case "LIST":
                    listPlaces(request,response);
                    break;
                case "ADD":
                    addPlaces(request,response);
                    break;
                case "LOAD":
                    loadPlaces(request,response);
                    break;
                case "UPDATE":
                    updatePlaces(request,response);
                    break;
                case "DELETE":
                    deletePlaces(request,response);
                    break;
                default:
                    listPlaces(request, response);
            }

        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    /**
     * Method used to transfer data over the network. Used to send data via forms.
     * Enables admin login.
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        String name = request.getParameter("loginInput");
        String password = request.getParameter("passwordInput");

        dbUtil.setName(name);
        dbUtil.setPassword(password);

        if(validate(name, password)) {

            RequestDispatcher dispatcher = request.getRequestDispatcher("/admin_view.jsp");
            List<Place> placeList = null;

            try {
                placeList = dbUtil.getPlace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            request.setAttribute("PLACES_LIST", placeList);
            dispatcher.forward(request,response);
        } else {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/admin_login_incorrect.html");
            dispatcher.include(request,response);
        }
    }

    /**
     * Validates connection for user.
     *
     * @param name username
     * @param password password
     * @return true if user has rights to connection
     */
    private boolean validate(String name, String password) {

        boolean status = false;
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url,name, password);
            status = true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return status;
    }

    /**
     * Method lists places in admin view.
     *
     * @param request
     * @param response
     * @throws Exception
     */
    private void  listPlaces(HttpServletRequest request, HttpServletResponse response)
    throws Exception{
        List<Place> placesList = dbUtil.getPlace();
        request.setAttribute("PLACES_LIST", placesList);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/admin_view.jsp");
        dispatcher.forward(request,response);
    }

    /**
     * Method used to add a new place from form.
     *
     * @param request
     * @param response
     * @throws Exception
     */
    private void addPlaces(HttpServletRequest request, HttpServletResponse response) throws Exception{

        String name = request.getParameter("nameInput");
        String type = request.getParameter("typeInput");
        String street = request.getParameter("streetInput");
        String city = request.getParameter("cityInput");
        String zip = request.getParameter("zipInput");
        String country = request.getParameter("countryInput");
        String phone = request.getParameter("phoneInput");
        String email = request.getParameter("emailInput");
        String description = request.getParameter("descriptionInput");

        Place place = new Place(name, type, street, city, zip, country, phone, email, description);
        dbUtil.addPlace(place);

        listPlaces(request,response);

    }

    /**
     * Method used to load specific place to update form.
     *
     * @param request
     * @param response
     * @throws Exception
     */
    private void loadPlaces(HttpServletRequest request, HttpServletResponse response) throws Exception{
        String id = request.getParameter("placeID");
        Place place = dbUtil.getPlace(id);

        request.setAttribute("PLACE", place);

        RequestDispatcher dispatcher = request.getRequestDispatcher("/update_place_form.jsp");
        dispatcher.forward(request,response);

    }

    /**
     * Method used to update specific place from form.
     *
     * @param request
     * @param response
     * @throws Exception
     */
    private  void updatePlaces(HttpServletRequest request, HttpServletResponse response) throws  Exception {
        int id = Integer.parseInt(request.getParameter("placeID"));
        String name = request.getParameter("nameInput");
        String type = request.getParameter("typeInput");
        String street = request.getParameter("streetInput");
        String city = request.getParameter("cityInput");
        String zip = request.getParameter("zipInput");
        String country = request.getParameter("countryInput");
        String phone = request.getParameter("phoneInput");
        String email = request.getParameter("emailInput");
        String description = request.getParameter("descriptionInput");

        Place place = new Place(id, name, type, street, city, zip, country, phone, email, description);
        dbUtil.updatePlace(place);
        listPlaces(request, response);

    }

    /**
     * Method used to delete specific place by a user.
     *
     * @param request
     * @param response
     * @throws Exception
     */
    private void deletePlaces(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String id = request.getParameter("placeID");
        dbUtil.deletePlace(id);
        listPlaces(request,response);
    }
}
