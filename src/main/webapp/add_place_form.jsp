<%--
  Created by IntelliJ IDEA.
  User: macmini2
  Date: 08/04/2020
  Time: 14:48
  To change this template use File | Settings | File Templates.
--%>
<!doctype html>
<html lang="en">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <meta charset="UTF-8">
    <title>Admin log panel</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="index.html">PetFriendly</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor03"
                aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor03">
            <ul class="navbar-nav me-auto">
                <li class="nav-item">
                    <a class="nav-link active" href="index.html">Home
                        <span class="visually-hidden">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="client_view.jsp">Check out available objects
                        <span class="visually-hidden">(current)</span>
                    </a>
                </li>
            </ul>

            <form class="d-flex">
                <a class="btn btn-primary btn-lg" href="admin_login.html" role="button">Admin</a>
            </form>
        </div>
    </div>
</nav>

<h1 style="margin-left: 10px; margin-top: 10px">Enter data:</h1>
<div class="container" style="width: 600px">
    <form action="AdminServlet" method="get">
        <input type="hidden" name="command" value="ADD">
        <div class="form-group">
            <label class="form-label mt-4" for="Name">Name</label>
            <input id="Name" type="text" class="form-control" name="nameInput" required placeholder="Enter name"
                   oninvalid="this.setCustomValidity('Fill this place')" oninput="this.setCustomValidity('')"/>
        </div>
        <div class="form-group">
            <label for="exampleSelect1" class="form-label mt-4">Type</label>
            <select class="form-select" id="exampleSelect1" name="typeInput" required oninvalid="this.setCustomValidity('Fill this place')" oninput="this.setCustomValidity('')">
                <option value="">Select type...</option>
                <option>Hotel</option>
                <option>Restaurant</option>
                <option>Bar</option>
                <option>Caf&eacute;</option>
                <option>Hairdresser</option>
                <option>Motel</option>
                <option>Shop</option>
                <option>Shopping mall</option>
                <option>Grocery store</option>
                <option>Supermarket</option>
                <option>Library</option>
                <option>Other</option>
            </select>
        </div>
        <br>
        <div class="form-group">
            <label for="Street">Address</label>
            <input style="margin-top: 6px" id="Street" type="text" class="form-control" name="streetInput" required placeholder="Enter street address"
                   oninvalid="this.setCustomValidity('Fill this place')" oninput="this.setCustomValidity('')"/>
            <input style="margin-top: 6px" id="City" type="text" class="form-control" name="cityInput" required placeholder="Enter city"
                   oninvalid="this.setCustomValidity('Fill this place')" oninput="this.setCustomValidity('')"/>
            <input style="margin-top: 6px" id="Zip" type="text" class="form-control" name="zipInput" required placeholder="Enter zip code"
                   oninvalid="this.setCustomValidity('Fill this place')" oninput="this.setCustomValidity('')"/>
            <input style="margin-top: 6px" id="Country" type="text" class="form-control" name="countryInput" required placeholder="Enter country"
                   oninvalid="this.setCustomValidity('Fill this place')" oninput="this.setCustomValidity('')"/>
        </div>
        <div class="form-group">
            <label for="email" class="form-label mt-4">Email address</label>
            <input type="email" class="form-control" id="email" placeholder="Enter email" name="emailInput" oninvalid="this.setCustomValidity('Incorrect email format.\nShould be something@example.com.')" oninput="this.setCustomValidity('')">
        </div>
        <br>
        <div class="form-group">
            <label for="Phone">Phone number</label>
            <input id="Phone" type="tel" pattern="\+[0-9]{7,15}" class="form-control" name="phoneInput" placeholder="Enter phone number" oninvalid="this.setCustomValidity('Incorrect format.\nShould be be +xx xxx xxx xxx.')" oninput="this.setCustomValidity('')"/>
        </div>
        <div class="form-group">
            <label for="exampleTextarea" class="form-label mt-4">Description</label>
            <textarea class="form-control" id="exampleTextarea" rows="3" name="descriptionInput"></textarea>
        </div>
        <br>
        <br>
        <button type="submit" class="btn btn-info">Add place</button>
    </form>
</div>

<div class="row">
    <div class="container-fluid">

        <div class="col-sm-9">
            <a href="AdminServlet" class="btn btn-lg btn-primary" role="button" aria-disabled="true">Go back</a>
        </div>
    </div>
</div>

</body>
</html>
