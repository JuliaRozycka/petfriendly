<%--
  Created by IntelliJ IDEA.
  User: Julia
  Date: 10/04/2020
  Time: 13:38
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"
         import="java.util.*,edu.ib.petfriendlyproject.Place" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Admin panel</title>
</head>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/popup.css">

<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="index.html">PetFriendly</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor03"
                aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor03">
            <ul class="navbar-nav me-auto">
                <li class="nav-item">
                    <a class="nav-link active" href="index.html">Home
                        <span class="visually-hidden">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="about_us.html">About us
                        <span class="visually-hidden">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="contact.html">Contact
                        <span class="visually-hidden">(current)</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="row form-group"></div>
<div class="row form-group"></div>
<div class="row form-group"></div>
<div class="row form-group"></div>


<h1>Places</h1>

<div class="row form-group"></div>
<div class="row form-group"></div>
<div class="row form-group"></div>

<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">No.</th>
        <th scope="col">Name</th>
        <th scope="col">Type</th>
        <th scope="col">Address</th>
        <th scope="col">Email</th>
        <th scope="col">Phone</th>
        <th scope="col">Description</th>
        <th scope="col">Action</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="tmpPlace" items="${PLACES_LIST}">
        <c:url var="updateLink" value="AdminServlet">
            <c:param name="command" value="LOAD"></c:param>
            <c:param name="placeID" value="${tmpPlace.id}"></c:param>
        </c:url>
        <c:url var="deleteLink" value="AdminServlet">
            <c:param name="command" value="DELETE"></c:param>
            <c:param name="placeID" value="${tmpPlace.id}"></c:param>
        </c:url>
        <tr>
            <th scope="row">${tmpPlace.id}</th>
            <td>${tmpPlace.name}</td>
            <td>${tmpPlace.type}</td>
            <td>${tmpPlace.street}, ${tmpPlace.city} ${tmpPlace.zip}, ${tmpPlace.country}</td>
            <td>${tmpPlace.email}</td>
            <td>${tmpPlace.phone}</td>
            <td>${tmpPlace.description}</td>
            <td><a href="${updateLink}">
                <button type="button" class="btn btn-success" title="Edit object"><img
                        src="img/outline_edit_black_24dp.png" alt="edit_button"></button>
            </a>
                <a href="${deleteLink}"
                   onclick="if(!(confirm('Are you sure you want to delete this position?'))) return false">
                    <button type="button" class="btn btn-danger" style="margin-top: 6px" title="Delete object"><img
                            src="img/outline_delete_black_24dp.png" alt="delte button"></button>
                </a></td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<div class="row form-group"></div>
<div class="row form-group"></div>
<div class="row form-group"></div>

<div style="margin-left: 10px">
    <p><a class="btn btn-lg btn-primary" href="add_place_form.jsp" role="button" title="Add new place"><img
            src="img/outline_playlist_add_black_24dp.png" alt="add button"></a></p>
</div>
<div style="margin-left: 10px; margin-top: 10px">
    <p><a class="btn btn-lg btn-secondary" href="#popup1" title="Info"><img
            src="img/outline_help_outline_black_24dp.png" alt="help button"></a></p>
</div>
<div id="popup1" class="overlay">
    <div class="popup">
        <h2>Hello Admin!</h2>
        <a class="close" href="#">&times;</a>
        <div class="content">
            To edit a place hit green edit button ->
            <button type="button" class="btn btn-success" style="width: 50px; height: 50px; border-radius: 8px"><img
                    style="align-content: center" src="img/outline_edit_black_24dp.png" alt="edit_button"></button>
            <br>
            To add a place click on the blue add button ->
            <button type="button" class="btn btn-lg btn-primary" style="width: 50px; height: 50px;border-radius: 8px">
                <img style="align-content: center" src="img/outline_playlist_add_black_24dp.png" alt="add button">
            </button>
            <br>
            To delete click on the red button ->
            <button class="btn btn-danger" style="width: 50px; height: 50px; border-radius: 8px"><img
                    src="img/outline_delete_black_24dp.png" style="align-content: center" alt="delte button"></button>
        </div>
    </div>
</div>

<div class="row form-group"></div>
<div class="row form-group"></div>
<div class="row form-group"></div>
</body>
</html>